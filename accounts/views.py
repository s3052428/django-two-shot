from django.shortcuts import redirect, render
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
# Create your views here.

# Function Based Signup View
# def signup_user(request):
#     if request.method == 'POST':
#         form = UserCreationForm()
#         if form.is_valid():
#             form.save()
#             return redirect("home")
#         else:
#             form = UserCreationForm()
#     form = UserCreationForm()  
#     context = {
#         'form':form,
#     }
#     return render(request, "signup.html", context)

class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("home")
    template_name = "signup.html"