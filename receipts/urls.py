from django.urls import path
from receipts.views import (
    ReceiptCreateView,
    ReceiptsListView,
    ExpenseCategoryList,
    AccountList,
    ExpenseCategoryCreateView,
    AccountCreateView,
)

urlpatterns = [
    path('', ReceiptsListView.as_view(), name="home"),
    path('create/', ReceiptCreateView.as_view(), name="create_receipt"),
    path('categories/create/', ExpenseCategoryCreateView.as_view(), name="create_category"),
    path('categories/', ExpenseCategoryList.as_view(), name="list_category"),
    path('accounts/create/', AccountCreateView.as_view(), name="create_account"),
    path('accounts/', AccountList.as_view(), name="list_accounts"),
]
