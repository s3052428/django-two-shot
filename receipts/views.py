from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.shortcuts import redirect
from receipts.models import Receipt, ExpenseCategory, Account

# Create your views here.


# It inherits from CreateView because it's about creating things
class ReceiptsListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/receipts_list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


# It inherits from CreateView because it's about creating things
class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/create_receipt.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        form.instance.purchaser = self.request.user
        return super().form_valid(form)


class ExpenseCategoryList(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/expense_category_list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/create_category.html"
    fields = ["name"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_category")


class AccountList(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/account_list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/create_account.html"
    fields = ['name', 'number']

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_accounts")